const { PrismaClient } = require('@prisma/client')
const { parse } = require("uuid")
const prisma = new PrismaClient({})

const uuidToBuffer = (uuid) => {
    return Buffer.from(parse(uuid));
};

const fetchVideoDetailsMethod=async(videoId)=>{

    const videoData = await prisma.videos_db.findFirst({
        where:{
            video_id:uuidToBuffer(videoId)
        }
    })
    return videoData.state

}


module.exports=fetchVideoDetailsMethod