const { PrismaClient } = require('@prisma/client');
const { default: axios } = require('axios');
const { parse } = require("uuid")
const prisma = new PrismaClient({})
require('dotenv').config();
const Constants = require('./Constants');

const uuidToBuffer = (uuid) => {
    return Buffer.from(parse(uuid));
};

const uploadVideoDetails = async (userId, videoId, title, description, state, createdAt) => {

    try {
        const video = await prisma.videos_db.create({
            data: {
                user_id: uuidToBuffer(userId),
                video_id: uuidToBuffer(videoId),
                title: title,
                description: description,
                created_at: createdAt,
                state: state
            }
        });

        let success;
        if (video !== null) {
            success = true
            const initializeResponse = await axios.post(Constants.VIDEO_ENGAGEMENT_URL, {}, {
                headers: {
                    videoId: videoId
                }
            })

            const dataForInitializVideo = await initializeResponse.data
            if (dataForInitializVideo.success) {
                return { success: success, msg: "Video data has been saved and engagement initialized" }
            }
            else {
                return { success: false, msg: "Video data has been saved but engagement has not been initialized" }
            }
        }
        else {
            success = false
        }

        return { success: success, msg: "Video data has been saved" }
    } catch (error) {
        return { success: false, msg: error.message }
    }
}

module.exports = uploadVideoDetails