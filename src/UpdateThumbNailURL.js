const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient({})
const { parse } = require("uuid")

const uuidToBuffer = (uuid) => {
    return Buffer.from(parse(uuid));
};

const updateThumbNailURL = async (userId, videoId, bucketURL) => {

    try {
        const thumbNailUpdate = await prisma.videos_db.update({
            where: {
                user_id: uuidToBuffer(userId),
                video_id: uuidToBuffer(videoId)
            },
            data: {
                thumb_nailurl: `${bucketURL}/${userId}/${videoId}/${videoId}`
            }
        })

        let success;
        if (thumbNailUpdate !== null) {
            success = true
        }
        else {
            success = false
        }

        return { success: success, msg: "Video URL has been updated" }
    } catch (error) {
        return { success: false, msg: error.message }
    }

}

module.exports = updateThumbNailURL