const express = require('express');
const { ECSClient, RunTaskCommand, LaunchType, AssignPublicIp } = require("@aws-sdk/client-ecs");
const Constants = require('./Constants');

const app = express();
app.use(express.json())

const ecsClient = new ECSClient({
    region: Constants.REGION,
    credentials: {
        accessKeyId: Constants.AWS_ACCESS_KEY_ID,
        secretAccessKey: Constants.AWS_SECRET_ACCESS_KEY
    }
});

const runTaskService = async (userId, videoId, videoName) => {

    try {
        const subnets_list = Constants.SUBNETS.split(',')

        const command = new RunTaskCommand({
            cluster: Constants.CLUSTER,
            taskDefinition: Constants.TASK_DEFINITION,
            launchType: LaunchType.FARGATE,
            count: 1,
            networkConfiguration: {
                awsvpcConfiguration: {
                    assignPublicIp: AssignPublicIp.ENABLED,
                    subnets: subnets_list,
                    securityGroups: [Constants.SECURITY_GROUPS]
                }
            },
            overrides: {
                containerOverrides: [
                    {
                        name: "transcoder-cont",
                        environment: [
                            { name: "TEMP_BUCKET_NAME", value: Constants.TEMP_BUCKET_NAME },
                            { name: "AWS_ACCESS_KEY_ID", value: Constants.AWS_ACCESS_KEY_ID },
                            { name: "AWS_SECRET_ACCESS_KEY", value: Constants.AWS_SECRET_ACCESS_KEY },
                            { name: "USER_ID", value: userId },
                            { name: "VIDEO_ID", value: videoId },
                            { name: "PERM_BUCKET_NAME", value: Constants.PERM_BUCKET_NAME },
                            { name: "VIDEO_NAME", value: videoName }
                        ]
                    }
                ]
            }
        })

        await ecsClient.send(command)

        return { success: true, msg: "Transcoder's running." }

    } catch (error) {
        return { success: false, msg: error }
    }
}

module.exports = runTaskService

