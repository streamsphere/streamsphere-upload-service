const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient({})
const { parse, stringify } = require("uuid")
const { s3Client } = require('./S3Data/S3Client')
const { DeleteObjectsCommand, ListObjectsV2Command, DeleteObjectCommand } = require('@aws-sdk/client-s3');
const Constants = require('./Constants');
require('dotenv').config();

const uuidToBuffer = (uuid) => {
    return Buffer.from(parse(uuid));
};

const bufferToUUID = (buffer) => {
    return stringify(Buffer.from(buffer));
};

const listObjectsInFolder = async (bucketName, folderPath) => {
    const listParams = {
        Bucket: bucketName,
        Prefix: folderPath,
    };

    const command = new ListObjectsV2Command(listParams);
    const response = await s3Client.send(command);
    return response.Contents.map(item => ({ Key: item.Key }));
};

const deleteObjectsFromS3 = async (userId, videoId) => {
    try {
        const bucketName = Constants.PERM_BUCKET_NAME;
        const objectsToDelete = await listObjectsInFolder(bucketName, `${userId}/${videoId}`);

        if (objectsToDelete.length === 0) {
            return { success: false, message: "Video not found" }
        }

        const deleteCommand = new DeleteObjectsCommand({
            Bucket: bucketName,
            Delete: {
                Objects: objectsToDelete
            }
        });

        const thumbNailDeleteCommand = new DeleteObjectCommand({
            Bucket: Constants.THUMBNAIL_BUCKET_NAME,
            Key: `${userId}/${videoId}/${videoId}`
        })

        await s3Client.send(deleteCommand);
        await s3Client.send(thumbNailDeleteCommand);

        return { success: true, message: "Video has been deleted from S3" }
    } catch (error) {
        return { success: false, message: `Internal Server Error. Reason: ${error.message}` }
    }
}

const deleteVideoMethod = async (userId, videoId) => {
    deleteObjectsFromS3(userId, videoId)

    const videoDetails = await prisma.videos_db.findFirst({
        where: {
            video_id: uuidToBuffer(videoId)
        }
    })

    if (videoDetails === null) {
        return { success: false, message: "Video not found" }
    }

    if (bufferToUUID(videoDetails.user_id) !== userId) {
        return { success: false, message: "Unauthorized to delete the video" }
    }

    const responseForDeletion = await deleteObjectsFromS3(bufferToUUID(videoDetails.user_id), bufferToUUID(videoDetails.video_id))

    if (responseForDeletion.success) {
        await prisma.videos_db.delete({
            where: {
                video_id: uuidToBuffer(videoId),
                user_id: uuidToBuffer(userId)
            }
        })
        return { success: true, message: "Video has been deleted" }
    }
    else {
        return responseForDeletion;
    }

}


module.exports = deleteVideoMethod;