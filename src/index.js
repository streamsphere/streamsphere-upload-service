const express = require('express');
const cors = require('cors');
const { S3Client, PutObjectCommand } = require('@aws-sdk/client-s3');
const { getSignedUrl } = require('@aws-sdk/s3-request-presigner');
require('dotenv').config();
const { v4: uuidv4 } = require('uuid');
const uploadVideoDetails = require('./UploadVideoDetailsService')
const runTaskService = require('./TriggerECSService')
const { updateVideoURL, updateVideoState } = require('./UpdateVideoURL');
const updateThumbNailURL = require('./UpdateThumbNailURL');
const fetchVideoDetailsMethod = require('./FetchVideoDetails')
const deleteVideoMethod = require('./DeleteVideoService')
const Constants = require('./Constants');
const channelAvatarUpload = require('./ChannelOps/ChannelAvatarUpload');
const updateChannelAvatarURLMethod = require('./ChannelOps/UpdateChannelAvatar');

const app = express();
app.use(express.json());

app.use(cors("*"));

const port = 3000;

const s3Client = new S3Client({
    region: Constants.REGION,
    credentials: {
        accessKeyId: Constants.AWS_ACCESS_KEY_ID,
        secretAccessKey: Constants.AWS_SECRET_ACCESS_KEY,
    },
});

const putObjectFunc = async (fileName, contentType) => {
    const command = new PutObjectCommand({
        Bucket: Constants.TEMP_BUCKET_NAME,
        Key: `${fileName}`,
        ContentType: contentType,
    });

    const url = await getSignedUrl(s3Client, command);
    return url;
};

app.get('/getputurl', async (req, res) => {
    const userId = req.headers['userid'];
    const videoTypeExt = req.headers['videotypeext'];
    const videoType = req.headers['videotype']
    const videoId = uuidv4();

    if (!userId || !videoId) {
        return res.status(400).send({ error: 'Missing userId or videoId in headers' });
    }

    try {
        const s3_url = await putObjectFunc(`${userId}/${videoId}/${videoId}.${videoTypeExt}`, videoType);
        return res.send({ url: s3_url, userId, videoId });
    } catch (error) {
        return res.status(500).send({ error: 'Error getting signed URL' });
    }
});

app.post('/uploadvideodetails', async (req, res) => {
    const userId = req.headers['userid'];
    const videoId = req.headers['videoid'];
    const state = req.headers['state'];
    const createdAt = req.headers['createdat'];

    const { title, description } = req.body;

    const result = await uploadVideoDetails(userId, videoId, title, description, state, createdAt);

    if (result.success) {
        res.send({ success: true, msg: result.msg });
    } else {
        res.status(500).send({ success: false, msg: result.msg, error: result.msg });
    }
})

app.post('/updatevideourl', async (req, res) => {
    const userId = req.headers['userid'];
    const videoId = req.headers['videoid'];
    const videoURL = req.headers['videourl']

    const result = await updateVideoURL(userId, videoId, videoURL)
    if (result.success) {
        res.send({ success: true, msg: result.msg });
    } else {
        res.status(500).send({ success: false, msg: 'Failed to store video url', error: result.msg });
    }
})

app.post('/updatevideostate', async (req, res) => {
    const userId = req.headers['userid'];
    const videoId = req.headers['videoid'];
    const newState = req.headers['state']

    const result = await updateVideoState(userId, videoId, newState)
    if (result.success) {
        res.send({ success: true, msg: result.msg });
    } else {
        res.status(500).send({ success: false, msg: 'Failed to update video state', error: result.msg });
    }
})

app.post('/triggerecs', async (req, res) => {
    const userId = req.headers['userid'];
    const videoId = req.headers['videoid'];
    const videoName = req.headers['videoname'];

    const msg = await runTaskService(userId, videoId, videoName);

    res.send({ msg: msg })
})

app.post('/uploadthumbnail', async (req, res) => {
    const userId = req.headers['userid'];
    const videoId = req.headers['videoid'];

    if (!userId || !videoId) {
        return res.status(400).send({ message: 'User ID and Video ID are required' });
    }

    try {
        const command = new PutObjectCommand({
            Bucket: Constants.THUMBNAIL_BUCKET_NAME,
            Key: `${userId}/${videoId}/${videoId}`
        });

        const url = await getSignedUrl(s3Client, command, { expiresIn: 3600 });
        res.status(200).send({ url });
    } catch (error) {
        res.status(500).send({ message: 'Error generating pre-signed URL', error: error.message });
    }
});

app.post('/updatethumbnailurl', async (req, res) => {
    const userId = req.headers['userid'];
    const videoId = req.headers['videoid'];
    const bucketURL = Constants.BUCKET_URL;

    const result = await updateThumbNailURL(userId, videoId, bucketURL);

    if (result.success) {
        res.send({ success: true, msg: result.msg });
    } else {
        res.status(500).send({ success: false, msg: 'Failed to store thumb nail url', error: result.msg });
    }
})

app.get('/getvideostate', async (req, res) => {

    const videoId = req.headers['videoid']
    const state = await fetchVideoDetailsMethod(videoId);
    return res.json({ state })
})


app.delete('/deletevideo', async (req, res) => {
    const userId = req.headers['userid'];
    const videoId = req.headers['videoid'];

    const response = await deleteVideoMethod(userId, videoId);
    res.send(response);

})

app.get('/getuploadchannelavatar', async (req, res) => {
    const userId = req.headers['userid'];
    const imageTypeExt = req.headers['imagetypeext'];
    const imageType = req.headers['imagetype']

    const response = await channelAvatarUpload(userId, imageTypeExt, imageType);
    res.status(200).send(response);
})

app.post('/updatechannelavatarurl', async (req, res) => {
    const userId = req.headers['userid'];
    const avatarKey = req.headers['avatarkey']

    const response = await updateChannelAvatarURLMethod(avatarKey, userId);
    res.send(response);
})

app.listen(port, () => {
    console.log(`Server running on PORT ${port}`);
});
