const { S3Client } = require('@aws-sdk/client-s3');
const Constants = require('../Constants');

const s3Client = new S3Client({
    region: Constants.REGION,
    credentials: {
        accessKeyId: Constants.AWS_ACCESS_KEY_ID,
        secretAccessKey: Constants.AWS_SECRET_ACCESS_KEY,
    },
});

module.exports = { s3Client };
