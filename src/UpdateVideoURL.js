const { PrismaClient } = require('@prisma/client')
const { parse } = require("uuid")
const prisma = new PrismaClient({})

const uuidToBuffer = (uuid) => {
    return Buffer.from(parse(uuid));
};

const updateVideoURL = async (userId, videoId, videoURL) => {
    try {
        const video = await prisma.videos_db.update({
            where: {
                user_id: uuidToBuffer(userId),
                video_id: uuidToBuffer(videoId)
            },
            data: {
                videourl: videoURL
            }
        })
        let success;
        if (video !== null) {
            success = true
        }
        else {
            success = false
        }

        return { success: success, msg: "Video URL has been updated" }
    } catch (error) {
        return { success: false, msg: error.message }
    }
}

const updateVideoState = async (userId, videoId, newState) => {
    try {
        const video = await prisma.videos_db.update({
            where: {
                user_id: uuidToBuffer(userId),
                video_id: uuidToBuffer(videoId)
            },
            data: {
                state: newState
            }
        })
        let success;
        if (video !== null) {
            success = true
        }
        else {
            success = false
        }

        return { success: success, msg: "Video state has been updated" }
    } catch (error) {
        return { success: false, msg: error.message }
    }
}

module.exports = {
    updateVideoURL,
    updateVideoState
}
