const Constants = require('../Constants');
const prismaDB = require('../DatabaseOps/db')
const { parse, stringify } = require("uuid")

const uuidToBuffer = (uuid) => {
    return Buffer.from(parse(uuid));
};

const bufferToUUID = (buffer) => {
    return stringify(Buffer.from(buffer));
};

const updateChannelAvatarURLMethod = async (avatarKey, userId) => {

    try {
        const userDetails = await prismaDB.prisma.user_db.findFirst({
            where: {
                user_id: uuidToBuffer(userId)
            }
        })

        if (userDetails === null) {
            return { success: false, message: "User not found." }
        }

        if (bufferToUUID(userDetails.user_id) !== userId) {
            return { success: false, message: "Unauthorized to update the avatar url" }
        }

        const updateResponse = await prismaDB.prisma.user_db.update({
            where: {
                user_id: userDetails.user_id
            },
            data: {
                avatarurl: `${Constants.AVATAR_BUCKET_URL}/${avatarKey}`
            }
        })

        if (updateResponse === null) {
            return { success: false, message: "Error while updating the avatar url" }
        }

        return { success: true, message: "Avatar URL updated successfully" }
    } catch (error) {
        return { success: false, message: `Internal Server Error. Reason: ${error.message}` }
    }

}

module.exports = updateChannelAvatarURLMethod;