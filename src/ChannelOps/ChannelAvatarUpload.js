const { PutObjectCommand } = require('@aws-sdk/client-s3')
const { getSignedUrl } = require('@aws-sdk/s3-request-presigner');
const {s3Client} = require('../S3Data/S3Client')
const Constants = require('../Constants')

const putObjectFunc = async (fileName, contentType) => {
    const command = new PutObjectCommand({
        Bucket: Constants.CHANNEL_AVATAR_BUCKET_NAME,
        Key: `${fileName}`,
        ContentType: contentType,
    });

    const url = await getSignedUrl(s3Client, command);
    return url;
};

const channelAvatarUpload = async (userId, imageTypeExt, contentType) => {

    try {
        const avatarUploadURL = await putObjectFunc(`${userId}.${imageTypeExt}`, contentType);
        return { success: true, message: avatarUploadURL };
    } catch (error) {
        return { success: false, message: `Internal Server Error. Reason: ${error.message}` }
    }

}

module.exports = channelAvatarUpload