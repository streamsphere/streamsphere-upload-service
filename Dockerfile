FROM ubuntu:latest

RUN apt-get update
RUN apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_20.x | bash -
RUN apt-get upgrade -y
RUN apt-get install -y nodejs

WORKDIR /home/app/

COPY . .

RUN npm install

ENTRYPOINT [ "node","src/index.js" ]